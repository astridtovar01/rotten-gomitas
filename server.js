const express = require("express");
const app = express();

const config = require("./config");

const cors = require("cors");
const bodyParser = require("body-parser");
const db = require("./db");
const router = require("./network/routes");

db(config.db_url);

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

router(app);

app.use(config.public_route, express.static("public"));

app.listen(config.port, () => {
  console.log(`La aplicación esta escuchando en ${config.host}:${config.port}`);
});
