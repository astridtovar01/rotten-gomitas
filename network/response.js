const config = require("../config");

exports.success = function (req, res, mensaje, status) {
  res.status(status || 200).send({
    error: 0,
    response: mensaje,
  });
};

exports.error = function (req, res, mensaje, status, detalles) {
  console.error(detalles);
  res.status(status || 500).send({
    error: 1,
    response: mensaje,
  });
};
