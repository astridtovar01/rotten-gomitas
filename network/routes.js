const usuario = require("../components/usuario/network");
const contenido = require("../components/contenido/network");
const gomatometro = require("../components/gomatometro/network");
const audiencia = require("../components/audiencia/network");

const routes = function (app) {
  app.use("/usuario", usuario);
  app.use("/contenido", contenido);
  app.use("/gomatometro", gomatometro);
  app.use("/audiencia", audiencia);
};

module.exports = routes;
