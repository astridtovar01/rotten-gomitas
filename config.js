const config = {
    // db_url: process.env.DB_URL || "mongodb://localhost:27017/rottengomitas",
    db_url: process.env.DB_URL || "mongodb+srv://user_rotten_gomitas:rottengomitas@cluster0.0wgku.gcp.mongodb.net/rottengomitas?retryWrites=true&w=majority",
    port: process.env.PORT || 3000,
    host: process.env.HOST || 'http://localhost',
    public_route: '/'
}

module.exports = config;