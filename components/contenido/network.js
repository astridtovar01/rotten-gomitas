const express = require("express");
const path = require("path");
const multer = require("multer");
const router = express.Router();
const controller = require("./controller");
const response = require("../../network/response");

const storage = multer.diskStorage({
  destination: "public/portadas/",
  filename: function (req, file, cb) {
    cb(
      null,
      file.fieldname + "-" + Date.now() + path.extname(file.originalname)
    );
  },
});

const upload = multer({ storage: storage });

router.post("/", upload.single("portada"), (req, res) => {
  const contenido = req.body;
  contenido.portada = req.file;
  controller
    .agregarContenido(contenido)
    .then((contenido) => {
      response.success(req, res, contenido);
    })
    .catch((e) => {
      response.error(req, res, e, 500, e);
    });
});

router.post("/agregarTemporada/:id", upload.single("portada"), function (req, res) {
  const temporada = req.body;
  temporada.portada = req.file;
  console.log(temporada);
  controller
    .agregarTemporadaSerie(req.params.id, temporada)
    .then((data) => {
      response.success(req, res, data, 200);
    })
    .catch((e) => {
      response.error(req, res, "Error interno", 500, e);
    });
});

router.post("/agregarCapitulo/:idTem", function (req, res) {
  const episodio = req.body;
  controller
    .agregarEposodioTemporada(req.params.idTem, episodio)
    .then((data) => {
      response.success(req, res, data, 200);
    })
    .catch((e) => {
      response.error(req, res, "Error interno", 500, e);
    });
});

router.get("/", function (req, res) {
  const idContenido = req.query.contenido || null;
  controller
    .listarContenido(idContenido)
    .then((listaContenido) => {
      response.success(req, res, listaContenido, 200);
    })
    .catch((e) => {
      response.error(req, res, "Error interno", 500, e);
    });
});

router.put("/:id", upload.single("portada"), function (req, res) {
  const contenido = req.body;
  if(req.file !== undefined){
    contenido.portada = req.file;
  }
  controller
    .editarContenido(req.params.id, contenido)
    .then((data) => {
      response.success(req, res, data, 200);
    })
    .catch((e) => {
      response.error(req, res, "Error interno", 500, e);
    });
});

module.exports = router;
