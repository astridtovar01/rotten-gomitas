const store = require("./store");
const config = require("../../config");

function agregarContenido(contenido) {
  return new Promise(async (resolve, reject) => {
    console.log(contenido);
    if (!contenido.nombre || !contenido.genero || !contenido.tipo || !contenido.portada) {
      console.log("[contenidoController] Data invalida.");
      return reject("Los datos son incorrectos.");
    }

    contenido.portada = `${config.host}:${config.port}/public/portadas/` + contenido.portada.filename;

    store.agregarContenido(contenido);
    resolve(contenido);
  });
}

function agregarTemporadaSerie(id, temporada) {
  return new Promise(async (resolve, reject) => {
    if (!id || !temporada.nombre) {
      console.log("[contenidoController] Data invalida.");
      return reject("Los datos son incorrectos.");
    }

    if (temporada.portada) {
      temporada.portada =
        `${config.host}:${config.port}/public/portadas/` +
        temporada.portada.filename;
    }

    const resultado = await store.agregarTemporadaSerie(id, temporada);
    resolve(resultado);
  });
}

function agregarEposodioTemporada(idTemporada, eposidio) {
  return new Promise(async (resolve, reject) => {
    if (!idTemporada || !eposidio.nombre) {
      console.log("[contenidoController] Data invalida.");
      return reject("Los datos son incorrectos.");
    }

    const resultado = await store.agregarEposodioTemporada(
      idTemporada,
      eposidio
    );
    resolve(resultado);
  });
}

function listarContenido(idContenido) {
  return new Promise(async (resolve, reject) => {
    resolve(store.listarContenido(idContenido));
  });
}

function editarContenido(id, contenido) {
  return new Promise(async (resolve, reject) => {
    if (!id || !contenido) {
      return reject("Los datos son incorrectos");
    }

    if (contenido.portada) {
      contenido.portada =
        `${config.host}:${config.port}/public/portadas/` +
        contenido.portada.filename;
    }

    const result = await store.editarContenido(id, contenido);
    resolve(result);
  });
}

module.exports = {
  agregarContenido,
  agregarTemporadaSerie,
  agregarEposodioTemporada,
  listarContenido,
  editarContenido
};
