const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const eposidioSchema = new Schema({
  nombre: {
    type: String,
    required: true,
  },
  sinopsis: {
    type: String,
  },
});

const temporadaSchema = new Schema({
  portada: {
    type: String,
  },
  nombre: {
    type: String,
    required: true,
  },
  episodios: [eposidioSchema],
});

const mySchema = new Schema({
  portada: {
    type: String,
    required: true,
  },
  nombre: {
    type: String,
    required: true,
  },
  sinopsis: {
    type: String,
  },
  genero: {
    type: String,
    required: true,
  },
  tipo: {
    type: String,
    required: true,
  },
  temporadas: [temporadaSchema],
  resenaCritico: [
    {
      type: Schema.ObjectId,
      ref: "gomatometro",
    },
  ],
  resenaAudiencia: [
    {
      type: Schema.ObjectId,
      ref: "audiencia",
    },
  ],
  promedioGomatometro: {
    type: Number,
    required: true,
    default: 0
  },
  promedioAudiencia: {
    type: Number,
    required: true,
    default: 0
  }
});

const model = mongoose.model("contenido", mySchema, "contenido");
module.exports = model;
