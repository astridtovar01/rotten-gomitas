const Model = require("./model");
const Gomatometro = require("../gomatometro/model");
const Audiencia = require("../audiencia/model");
const Usuario = require("../usuario/model");

function agregarContenido(contenido) {
  const nuevoContenido = new Model(contenido);
  nuevoContenido.save();
}

async function agregarTemporadaSerie(id, temporada) {
  const serieEncontrada = await Model.findOne({ _id: id });
  if (!temporada.portada) {
    temporada.portada = serieEncontrada.portada;
  }
  serieEncontrada.temporadas.push(temporada);
  return await serieEncontrada.save();
}

async function agregarEposodioTemporada(idTemporada, episodio) {
  const temporadaEncontrada = await Model.findOne({
    "temporadas._id": idTemporada,
  });
  for (i in temporadaEncontrada.temporadas) {
    let e = temporadaEncontrada.temporadas[i];
    if (e._id == idTemporada) {
      e.episodios.push(episodio);
    }
  }
  return await temporadaEncontrada.save();
}

async function listarContenido(idContenido) {
  return new Promise((resolve, reject) => {
    let filter = {};
    if (idContenido !== null) {
      filter = { _id: idContenido };
    }
    Model.find(filter)
      .populate(
        {
          path: "resenaCritico",
          model: Gomatometro,
          populate: { path: "usuario", model: Usuario },
        },
      ).populate(
        {
          path: "resenaAudiencia",
          model: Audiencia,
          populate: { path: "usuario", model: Usuario },
        },
      )
      .exec((err, populated) => {
        if (err) {
          return reject(err);
        }
        resolve(populated);
      });
  });
}

async function editarContenido(id, contenido) {
  return await Model.findOneAndUpdate({ _id: id }, contenido, {
    new: true
  });
}


module.exports = {
  agregarContenido,
  agregarTemporadaSerie,
  agregarEposodioTemporada,
  listarContenido,
  editarContenido
};
