const store = require("./store");
const storeContenido = require("../contenido/store");

function agregarResena(idContenido, resena) {
  return new Promise(async (resolve, reject) => {
    if (!idContenido || !resena.usuario || !resena.puntaje) {
      console.log("[audienciaController] Data invalida.");
      return reject("Los datos son incorrectos.");
    }

    store.agregarResena(idContenido, resena);

    resolve(resena);
  });
}

module.exports = {
  agregarResena,
};
