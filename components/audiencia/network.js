const express = require("express");
const router = express.Router();
const controller = require("./controller");
const response = require("../../network/response");

router.post("/:idContenido", (req, res) => {
  const resena = req.body;
  controller
    .agregarResena(req.params.idContenido, resena)
    .then((resena) => {
      response.success(req, res, resena);
    })
    .catch((e) => {
      response.error(req, res, e, 500, e);
    });
});

module.exports = router;
