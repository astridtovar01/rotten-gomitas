const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const mySchema = new Schema({
  cod: {
    type: Number,
    required: true,
  },
  nombre: {
    type: String,
    required: true,
  },
});

const model = mongoose.model("roles", mySchema, "roles");
module.exports = model;
