const store = require("./store");
const config = require("../../config");

function agregarResena(idContenido, resena) {
  return new Promise(async (resolve, reject) => {
    if (!idContenido || !resena.usuario || !resena.puntaje) {
      console.log("[GomatometroController] Data invalida.");
      return reject("Los datos son incorrectos.");
    }
    
    store.agregarResena(idContenido, resena);
    resolve(resena);
  });
}

module.exports = {
  agregarResena,
};
