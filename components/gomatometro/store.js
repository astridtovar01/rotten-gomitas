const Model = require("./model");
const ModelContenido = require("../contenido/model");

async function agregarResena(idContenido, resena) {
  return new Promise((resolve, reject) => {
    const nuevaResena = new Model(resena);
    nuevaResena.save(async (err, resenaRegistrada) => {
      if (err) {
        console.log(`[gomatormetroStore] ${err}`);
        return reject("Error interno.");
      }
      const contenido = await ModelContenido.findOne({ _id: idContenido });
      contenido.resenaCritico.push(resenaRegistrada._id);
      let resultado = await contenido.save();
      resolve(resultado);
    });
  });
}

module.exports = {
  agregarResena,
};
