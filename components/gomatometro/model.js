const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const mySchema = new Schema({
  puntaje: {
    type: Number,
    required: true,
  },
  comentario: {
    type: String,
  },
  usuario: {
    type: Schema.ObjectId,
    ref: "usuarios",
  },
});

const model = mongoose.model("gomatometro", mySchema, "gomatometro");
module.exports = model;
