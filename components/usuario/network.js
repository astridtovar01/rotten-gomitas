const express = require("express");
const router = express.Router();
const controller = require("./controller");
const response = require("../../network/response");

// Agregar usuarios
router.post("/", (req, res) => {
  const usuario = req.body.data;
  controller
    .agregarUsuario(usuario)
    .then((usuario) => {
      response.success(req, res, usuario);
    })
    .catch((e) => {
      response.error(req, res, e, 500, e);
    });
});

// Iniciar sesión
router.post("/iniciarSesion", (req, res) => {
  const usuario = req.body.data;
  controller
    .iniciarSesion(usuario)
    .then((usuario) => {
      response.success(req, res, usuario);
    })
    .catch((e) => {
      response.error(req, res, "Unexpected Error", 500, e);
    });
});

module.exports = router;
