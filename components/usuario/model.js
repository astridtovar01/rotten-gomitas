const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const mySchema = new Schema({
  nombre: {
    type: String,
    required: true,
  },
  correo: {
    type: String,
    required: true,
  },
  contrasena: {
    type: String,
    required: true,
  },
  rol: {
    type: Schema.ObjectId,
    ref: "roles",
  },
});

const model = mongoose.model("usuario", mySchema, "usuarios");
module.exports = model;
