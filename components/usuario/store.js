const Model = require("./model");
const Rol = require("../rol/model");

async function validarUsuarioExistente(correo) {
  const existeUsuario = Model.findOne({ correo: correo });
  return existeUsuario;
}

function agregarUsuario(usuario) {
  const nuevoUsuario = new Model(usuario);
  nuevoUsuario.save();
}

async function iniciarSesion(credenciales) {
  return new Promise((resolve, reject) => {
    const usuario = Model.find(credenciales)
      .populate({ path: 'rol', model: Rol })
      .exec((err, populated) => {
        if (err) {
          return reject(err);
        }
        resolve(populated);
      });
  });
}

module.exports = {
  agregarUsuario,
  validarUsuarioExistente,
  iniciarSesion,
};
