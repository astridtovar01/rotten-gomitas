const store = require("./store");

function agregarUsuario(usuario) {
  return new Promise(async (resolve, reject) => {
    if (!usuario.nombre || !usuario.correo || !usuario.contrasena || !usuario.rol) {
      console.log("[UsuarioController] Data invalida.");
      return reject("Los datos son incorrectos.");
    }

    const existeUsuario = await store.validarUsuarioExistente(usuario.correo);
    if (existeUsuario && existeUsuario !== null) {
      console.log("Usuario existe.");
      return resolve("Usuario existe.");
    }

    store.agregarUsuario(usuario);
    resolve(usuario);
  });
}

function iniciarSesion(credenciales) {
  return new Promise(async (resolve, reject) => {
    if (!credenciales) {
      console.log("[UsuarioController] Data invalida.");
      return reject("Los datos son incorrectos.");
    }

    const usuario = await store.iniciarSesion(credenciales);
    console.log(usuario)
    if (!usuario || usuario === null) {
      console.log("[UsuarioController] Credenciales invalidas.");
      return reject("Credenciales invalidas.");
    }

    resolve(usuario);
  })
}

module.exports = {
  agregarUsuario,
  iniciarSesion,
};
